(ns plf02.core)

(defn función-associative?-1
  [a]
  (associative? a))

(defn función-associative?-2
   [a]
  (associative? a))

(defn función-associative?-3
   [a]
  (associative? a))

(función-associative?-1 [5 10 15])
(función-associative?-2 5)
(función-associative?-3 '(5 10 15))


(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [a]
  (boolean? a))

(defn función-boolean?-3
  [a]
  (boolean? a))

(función-boolean?-1 (new Boolean "true"))
(función-boolean?-2 nil)
(función-boolean?-3 400)


(defn función-char?-1
  [a]
  (char? a))

(defn función-char?-2
  [a]
  (char? a))

(defn función-char?-3
  [a]
  (char? a))

(función-char?-1 \a)
(función-char?-2 12)
(función-char?-3 'a)


(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a]
  (coll? a))

(defn función-coll?-3
  [a]
  (coll? a))


(función-coll?-1 [1 2 3])
(función-coll?-2 3 )
(función-coll?-3 '(\a \b \c))


(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 1M)
(función-decimal?-2 1.0)
(función-decimal?-3 [4M])


(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a]
  (double? a))

(defn función-double?-3
  [a]
  (double? a))


(función-double?-1 1.0)
(función-double?-2 3)
(función-double?-3 [2.0 5.0 1.0 3.2 2])

(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a]
  (float? a))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 3.5)
(función-float?-2 10)
(función-float?-3 true)


(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a]
  (ident? a))

(función-ident?-1 'a)
(función-ident?-1 :a)
(función-ident?-3 '(c \a))


(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a]
  (indexed? a))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [10 20 30])
(función-indexed?-2 ["PLF" "isa"])
(función-indexed?-3 '([\a 2 \c {:a a :b b}]))


(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a]
  (int? a))

(defn función-int?-3
  [a ]
  (int? a))

(función-int?-1 25)
(función-int?-2 4.5)
(función-int?-3 5/4)


(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a]
  (integer?  a))

(defn función-integer?-3
  [a]
  (integer? a))

(función-integer?-1 50)
(función-integer?-2 5.5)
(función-integer?-3 [1 0 15 3/4])


(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))


(función-keyword?-1 :a)
(función-keyword?-2 [:a \a :b \b :c \c])
(función-keyword?-3 ":a")


(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))


(función-list?-1 '(10 20 30))
(función-list?-2  [40 80 120])
(función-list?-3 #{'(20 30) [40 60] [80 90]})


(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 #{1 2 3 4})
(función-map-entry?-2 {:c \C})
(función-map-entry?-3 {{3 2} {2 3} {6 1} {4 5}})


(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (int? a) )


(función-map?-1 {10 20 30 40 50 60})
(función-map?-2 [{:a 'a :b 'b} {:c 'c :d 'd}])
(función-map?-3 10000)



(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
  [a]
  (nat-int? a))

(función-nat-int?-1 -40)
(función-nat-int?-2 50)
(función-nat-int?-3 5/4)


(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 10)
(función-number?-2 '([\a \b]))
(función-number?-3 \@)


(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int?  a))

(función-pos-int?-1 12)
(función-pos-int?-2 10.5)
(función-pos-int?-3 [\a 1 \b 2])


(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a ]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))


(función-ratio?-1 4/4)
(función-ratio?-2 12 )
(función-ratio?-3 ["PLF" "ISA"])


(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 0/50)
(función-rational?-2 10.5 )
(función-rational?-3 66.6)


(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))

(función-seq?-1 '(1 2 3))
(función-seq?-2 10 )
(función-seq?-3 [10 20 30 40])


(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [20 30 40])
(función-seqable?-2 {{10 20} {30 40}})
(función-seqable?-3 50)


(defn función-sequential?-1
  [a]
  (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))


(función-sequential?-1 [10 20 30 40])
(función-sequential?-2 100)
(función-sequential?-3 #{[\a] [\b] [\c]})


(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))


(defn función-set?-3
  [a ]
  (set?  a))

(función-set?-1 {:a \a :b \b})
(función-set?-2 (hash-set 2 2 2) )
(función-set?-3 [[10 20] [30 40 50]])


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 false)
(función-some?-2 ["hola" "mundo"])
(función-some?-3 #{10 20 30 0 50})


(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "PLF")
(función-string?-2 (+ 5 6))
(función-string?-3 '(true false true))


(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))

(función-symbol?-1 \a)
(función-symbol?-2 'a)
(función-symbol?-3 45)



(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 [true false true false])
(función-vector?-2 '(10 20 30))
(función-vector?-3 "vector")





(defn función-drop-1
  [a b]
  (drop a b))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a b]
  (drop a b))

(función-drop-1 2 [10 20 30 40])
(función-drop-2 1 '([1 2] [3 4]))
(función-drop-3 4 {:a \a :b \b :c 8 :d \d :e \e})


(defn función-drop-last-1
  [a b]
  (drop-last a b))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

(función-drop-last-1 0 (list '(1 2 3 4)))
(función-drop-last-2 1 (vector 10 20 30 40))
(función-drop-last-3 2 (vector true false true \a))


(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [a b]
  (drop-while a b))

(defn función-drop-while-3
  [a b]
  (drop-while a b))

(función-drop-while-1 neg? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-2 #(>= 3 %) [1 2 3 4])
(función-drop-while-3 #(> 3 %) [1 2 3 4 5 6])


(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [a b]
  (every? a b))

(defn función-every?-3
  [a b]
  (every? a b))

(función-every?-1 even? '(10 20 30))
(función-every?-2 char? ["Cristofher"])
(función-every?-3 int? #{20 40 60 30})


(defn función-filterv-1
  [a b]
  (filterv a b))

(defn función-filterv-2
  [a b]
  (filterv a b))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 even? (range 50))
(función-filterv-2 odd? (range 40))
(función-filterv-3 pos? (range -3 3))


(defn función-group-by-1
  [a b]
  (group-by a b))

(defn función-group-by-2
  [a b]
  (group-by a b))

(defn función-group-by-3
  [a b]
  (group-by a b))

(función-group-by-1 count ["PLF" "ISA" "PLF02" "Clojure"])
(función-group-by-2 even? (range 10))
(función-group-by-3 :user-id
                    [{:user-id 1 :uri "/"}
                     {:user-id 2 :uri "/foo"}
                     {:user-id 1 :uri "/account"}])


(defn función-iterate-1
  [a b]
  (iterate a b))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (iterate a b))

(función-iterate-1 inc 5)
(función-iterate-2 inc 2)
(función-iterate-3 dec 2)


(defn función-keep-1
  [a b]
  (keep a b))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [a b]
  (keep a b))

(función-keep-1 even? (range 1 5))
(función-keep-2 odd? (range 20 21))
(función-keep-3 number? (range 30 33))


(defn función-keep-indexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-2
  [a b]
  (keep-indexed a b))

(defn función-keep-indexed-3
  [a b]
  (keep-indexed a b))

(función-keep-indexed-1 vector [:a :b :c :d])
(función-keep-indexed-2 list [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 hash-map [1 2 3 4 5 6])


(defn función-map-indexed-1
  [a b]
  (map-indexed a b))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [a b]
  (map-indexed a b))

(función-map-indexed-1  hash-set [:a :b :c])
(función-map-indexed-2  list [:a :b :c])
(función-map-indexed-3  hash-map "REPL")


(defn función-mapcat-1
  [a b]
  (mapcat a b))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [a b]
  (mapcat a b))

(función-mapcat-1  reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2  reverse [[1 2] [2 2] [2 3]])
(función-mapcat-3  (juxt inc dec) [1 2 3 4])


(defn función-mapv-1
  [a b]
  (mapv a b))

(defn función-mapv-2
  [a b c]
  (mapv a b c))

(defn función-mapv-3
  [a b]
  (mapv a b))

(función-mapv-1 inc [1 2 3 4 5])
(función-mapv-2 + [1 2 3] [4 5 6])
(función-mapv-3 dec [1 2 3])



(defn función-merge-with-1
  [a b]
  (merge-with a b))

(defn función-merge-with-2
  [a b c]
  (merge-with a b c))

(defn función-merge-with-3
  [a b c]
  (merge-with  a b c))

(función-merge-with-1 + {:a 9  :b 98  :c 0  :d 42})
(función-merge-with-2 merge{:x {:y 1}} {:x {:z 2}})
(función-merge-with-3 into {:a #{1 2 3}, :b #{4 5 6}} {:a #{2 3 7 8}, :c #{1 2 3}})


(defn función-not-any?-1
  [a b]
  (not-any? a b))

(defn función-not-any?-2
  [a b]
  (not-any? a b))

(defn función-not-any?-3
  [a b]
  (not-any? a  b))

(función-not-any?-1 odd? '(2 4 6))
(función-not-any?-2 nil? [true false false])
(función-not-any?-3 boolean? [true false nil])


(defn función-not-every?-1
  [a b]
  (not-every? a b))

(defn función-not-every?-2
  [a b]
  (not-every? a b))

(defn función-not-every?-3
  [a b]
  (not-every? a b))

(función-not-every?-1 odd? '(1 3 3 3))
(función-not-every?-2 even? '(2 5 6 10))
(función-not-every?-3 string? '("PLF" "ISA" 1))


(defn función-partition-by-1
  [a b]
  (partition-by a b))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [a b]
  (partition-by a b))

(función-partition-by-1 even? [1 1 1 2 2 3 3])
(función-partition-by-2 #(= 25 %) [10 20 25 30 40])
(función-partition-by-3 identity "Abadakadabra")


(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 assoc {} {:a 1 :b 2 :c 3})
(función-reduce-kv-2 assoc {} {:d 5 :e 4 :f 3})
(función-reduce-kv-3 vector [{:a 1 :b 2} ] [{:a 3 :b 4}])

(defn función-remove-1
  [a b]
  (remove a b))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a b]
  (remove a b))

(función-remove-1 pos? [-2 -1 0 1 2])
(función-remove-2 double? '(1 1.5 2 2.5 3 3.5))
(función-remove-3 even? (range 10))


(defn función-reverse-1
  [a]
  (reverse a))

(defn función-reverse-2
  [a]
  (reverse a))

(defn función-reverse-3
  [a]
  (reverse a))

(función-reverse-1 '(1 4 5 6))
(función-reverse-2 [\T \E \N \E \T])
(función-reverse-2 {'a' 'e' 'i' 'o'})


(defn función-some-1
  [a b]
  (some a b))

(defn función-some-2
  [a b]
  (some a b))

(defn función-some-3
  [a b]
  (some a b))

(función-some-1 even? [10 25 30 45])
(función-some-2 true? '(true false true))
(función-some-3 #(= 10 %) [20 30 40 50 10 60])

(defn función-sort-by-1
  [a b]
  (sort-by a b))

(defn función-sort-by-2
  [a b]
  (sort-by a b))

(defn función-sort-by-3
  [a b]
  (sort-by a b))

(función-sort-by-1 first ["a" "z" "g" "h"])
(función-sort-by-2 count '([1 2] [2 4 5] [3 2]))
(función-sort-by-3 last {\a \c \z \Z \b \B \t \V})


(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [a b]
  (split-with a b))

(defn función-split-with-3
  [a b]
  (split-with a b))

(función-split-with-1 (partial >= 30) [10 20 30 40 50])
(función-split-with-2 (partial > 3) [1 2 3 4 5])
(función-split-with-3 odd? [1 3 5 6 7 9])

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 1 [10 20 30])
(función-take-2 3 '(1 2 3 4 5 6))
(función-take-3 2 {\a \b \c \d})


(defn función-take-last-1
  [a b]
  (take-last a b))

(defn función-take-last-2
  [a b]
  (take-last a b))

(defn función-take-last-3
  [a b]
  (take-last a b))

(función-take-last-1 1 [10 20 30])
(función-take-last-2 3 '(1 2 3 4 5 6))
(función-take-last-3 2 {1 2 3 4 5 6})


(defn función-take-nth-1
  [a b]
  (take-nth a b))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [a b]
  (take-nth a b))

(función-take-nth-1 10 (range 100))
(función-take-nth-2 10 (range 20))
(función-take-nth-3 4 (range 20))


(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [a b]
  (take-while a b))

(defn función-take-while-3
  [a b]
  (take-while a b))

(función-take-while-1 neg? [-2 -1 0 1 2])
(función-take-while-2 #{[1 2] [3 4]} #{[3 4]})
(función-take-while-3 int? [1 2 3 1.5 2.5 3.5])


(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [a b c]
  (update a b c))

(defn función-update-3
  [a b c]
  (update a b c))

(función-update-1 [1 2 3] 0 inc)
(función-update-2 [10 20 30] 1 inc)
(función-update-3 [100 200 300] 1 inc)


(defn función-update-in-1
  [a b c]
  (update-in a b c))

(defn función-update-in-2
  [a b c d e]
  (update-in a b c d e))

(defn función-update-in-3
  [a b c]
  (update-in a b c))

(función-update-in-1 {1 {:b 3}} [1 :b] inc)
(función-update-in-2 {:a 3} [:a] * 2 2)
(función-update-in-3 [1 {:a 3 :b 3 :c 4}] [1 :c] (fnil dec 1) )